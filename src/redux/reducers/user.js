const initialState = {
    favorites: []
};

export const user = (state = initialState, action) => {

    switch (action.type) {

        case "DO_FAVORITES":

            const items = state.favorites;
            if (items && items.length > 0) {
                // console.log("0");
                const position = items.findIndex(
                    (item) => item === action.payload
                );
                // console.log(position);
                if (position === -1) {
                    // add other
                    // console.log("1");

                    return {
                        ...state,
                        favorites: [...state.favorites, action.payload],
                    };
                }
                else {
                    // same
                    // console.log("2");
                    const filtered = state.favorites.filter(function (value, index, arr) {
                        return index !== position;
                    });

                    return {
                        ...state,
                        favorites: filtered,
                    };
                }
            }
            else {
                // console.log("3");
                return {
                    ...state,
                    favorites: [...state.favorites, action.payload],
                };
            }
            return state;

        default:
            return state;

    }
};
