import React from 'react'
import { View, Text } from 'react-native'

import ItemMasonry from "./ItemMasonry"
import Styles from "../utilites/styles";

const Masonry = ({ dataLeft, dataRight }) => {

    if (dataLeft.length === 0 || dataRight.length === 0)
        return null;

    return (
        <>
            <View style={Styles.masonryContainer}>
                {/* Left column */}
                <View style={[Styles.masonryContainerColum, { width: "48%" }]}>
                    {
                        dataLeft.map((item, index) => {
                            return <ItemMasonry key={index} data={item} />
                        })
                    }
                </View>
                {/* End, Left column */}

                {/* Right column */}
                <View style={[Styles.masonryContainerColum, { width: "48%" }]}>
                    {
                        dataRight.map((item, index) => {
                            return <ItemMasonry key={index} data={item} />
                        })
                    }
                </View>
                {/* End, Right column */}
            </View>
        </>
    )
}

export default Masonry