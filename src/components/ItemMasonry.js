import React, { useEffect, useState } from 'react'
import { Image, TouchableOpacity, Text, View } from 'react-native'
import { useNavigation } from '@react-navigation/native';
import { Skeleton } from '@rneui/themed';

import Styles from "../utilites/styles"

import { NAVIGATION_SHOW } from "@ENV";

const ItemMasonry = ({ data }) => {

    const [img, setImg] = useState(data.image);
    const [isErrorToLoaded, setIsErrorToLoaded] = useState(false);
    const [isLoaded, setIsLoaded] = useState(false);

    const navigation = useNavigation();

    useEffect(() => {
        if (isErrorToLoaded) setImg(DEFAULT_PRODUCT);
        else if (isLoaded) setImg(data.image);
        else setImg(data.image);
    }, [isErrorToLoaded, isLoaded, data]);

    return (
        <TouchableOpacity
            activeOpacity={1}
            onPress={() => navigation.navigate(NAVIGATION_SHOW, { id: data.id })}
        >
            <View style={Styles.masonryItemBox}>
                <View style={Styles.masonryItemBoxThumb}>
                    <Image
                        onLoadEnd={(e) => {
                            setIsLoaded(true);
                        }}
                        onError={(e) => {
                            setIsErrorToLoaded(true);
                        }}
                        resizeMode="cover"
                        source={{ uri: img }}
                        style={Styles.masonryItemBoxThumbImg}
                    />
                </View>
                <View style={Styles.masonryItemBoxText}>
                    {isLoaded ?
                        (<Text>{data.title}</Text>)
                        : (
                            <>
                                <View style={{ marginBottom: 5 }} >
                                    <Skeleton height={10} width="90%" />
                                </View>
                                <View style={{ marginBottom: 5 }} >
                                    <Skeleton height={10} width="40%" skeletonStyle={{ marginBottom: 10 }} />
                                </View>
                                <Skeleton height={10} width="70%" />
                            </>)
                    }
                </View>
            </View>
        </TouchableOpacity >
    )
}

export default ItemMasonry