import React, { useEffect, useState } from 'react'
import { ScrollView, View, Text } from 'react-native'
import SafeAreaView from "react-native-safe-area-view";
import { Input } from "@rneui/themed";



import Masonry from '../components/Masonry';

import Styles from "../utilites/styles"

import { request } from "../utilites/request";

const Home = () => {

    const [dataLeft, setDataLeft] = useState([]);
    const [dataRight, setDataRight] = useState([]);
    const [keyWord, setKeyWord] = useState("");


    ////////////////////////////////////////// GET DATA
    useEffect(() => {
        if (keyWord && keyWord.length > 3)
            _request(keyWord)
        else
            _request("");
    }, [keyWord]);
    useEffect(() => {
        _request("");
    }, []);

    const _request = async (key) => {
        const query = key === "" ? '?q=apollo%2011' : '?q=apollo%2011&title=' + key;
        const response = await request("GET", query);

        const left = [];
        const right = [];
        let i = 0;

        response.data.collection.items.map((item, index) => {

            if (item.links) {
                i % 2 === 0 ?
                    left.push({
                        id: item.data[0].nasa_id,
                        image: item.links[0].href ? item.links[0].href : item.links[1].href,
                        title: item.data[0].title
                    })
                    :
                    right.push({
                        id: item.data[0].nasa_id,
                        image: item.links[0].href ? item.links[0].href : item.links[1].href,
                        title: item.data[0].title,
                    })

                i++;
            }

        })

        setDataLeft(left);
        setDataRight(right);
    }
    ////////////////////////////////////////// END, GET DATA



    // Object.entries(data).map(([key, value]) => (

    return (
        <SafeAreaView style={Styles.safeAreaView}>
            <ScrollView
                contentContainerStyle={Styles.scrollView}
                overScrollMode="never"
                scrollEventThrottle={16}
                showsVerticalScrollIndicator={false}
            >
                <View style={Styles.searchInput}>
                    <Input
                        autoCapitalize="none"
                        keyboardType="web-search"
                        onChangeText={(e) => setKeyWord(e)}
                        placeholder="Buscar"
                        selectTextOnFocus={false}
                        value={keyWord}
                    />
                </View>
                {dataLeft.length > 0 && (
                    < Masonry
                        dataLeft={dataLeft}
                        dataRight={dataRight}
                    />
                )}
            </ScrollView>
        </SafeAreaView>
    )
}

export default Home