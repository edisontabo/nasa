import React, { useEffect, useState } from 'react'
import { Image, ScrollView, TouchableOpacity, View } from 'react-native'
import SafeAreaView from "react-native-safe-area-view";
import { Text } from "@rneui/base";
import { Icon } from '@rneui/themed';
import { connect } from "react-redux";

import Styles from "../utilites/styles"

import { onFavorites } from "../redux/actions/user";

import { request } from "../utilites/request";

const Show = ({ navigation, onFavorites, route, favorites }) => {

    const { id } = route.params;

    const [title, setTitle] = useState("");
    const [img, setImg] = useState("");
    const [images, setImages] = useState([]);
    const [isFavorite, setIsFavorite] = useState(false);
    const [isErrorToLoaded, setIsErrorToLoaded] = useState(false);
    const [isLoaded, setIsLoaded] = useState(false);


    useEffect(() => {
        if (isErrorToLoaded) setImg(DEFAULT_PRODUCT);
        else if (isLoaded) setImg(images[0]);
        else setImg(images[0]);
    }, [isErrorToLoaded, isLoaded, images]);

    ////////////////////////////////////////// GET DATA
    useEffect(() => {
        _request(1);
    }, []);
    const _request = async (page) => {
        const query = "?q=apollo%2011&nasa_id=" + encodeURI(id)
        const response = await request("GET", query);

        const imgs = [];
        response.data.collection.items[0].links.map((item) => {
            item.render === "image" && imgs.push(encodeURI(item.href));
        })

        setImages(imgs);
        setTitle(response.data.collection.items[0].data[0].title ? response.data.collection.items[0].data[0].title : "")
    }
    ////////////////////////////////////////// END, GET DATA


    useEffect(() => {
        favorites.length == 0 && setIsFavorite(false);
        favorites.map((item) => {
            item == encodeURI(id) ?
                setIsFavorite(true)
                :
                setIsFavorite(false)
        })
    }, [favorites])

    return (
        <SafeAreaView style={Styles.safeAreaView}>
            <ScrollView
                contentContainerStyle={Styles.scrollView}
                overScrollMode="never"
                scrollEventThrottle={16}
                showsVerticalScrollIndicator={false}
            >
                {img !== "" && (
                    <Image source={{ uri: img }} style={{ height: 300, width: "100%" }} />
                )}
                <Text h1 >{title}</Text>

                <TouchableOpacity
                    activeOpacity={1}
                    onPress={() => onFavorites(encodeURI(id))}
                    style={{
                        backgroundColor: isFavorite ? "#FED700" : "#D3D3D3",
                        padding: 14,
                        borderRadius: 15
                    }}
                >
                    <Icon
                        color="#fff"
                        name={isFavorite === 1 ? "star" : "star-o"}
                        size={25}
                        type="font-awesome"
                    />
                </TouchableOpacity>
            </ScrollView>
        </SafeAreaView>
    )
}




const mapStateToProps = (state) => {
    return {
        favorites: state.user.favorites,
    };
};
export default connect(mapStateToProps, { onFavorites })(Show);