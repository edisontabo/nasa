import React from "react";
import { useNavigation } from '@react-navigation/native';
import { createNativeStackNavigator } from "@react-navigation/native-stack";

import Home from "../views/Home";
import Show from "../views/Show";


import {
    NAVIGATION_HOME,
    NAVIGATION_SHOW
} from "@ENV";

const Stack = createNativeStackNavigator();

const App = () => {

    const router = [
        {
            component: Home,
            name: NAVIGATION_HOME,
            options: {
                headerShown: false,
                headerTransparent: true
            },
        },
        {
            component: Show,
            name: NAVIGATION_SHOW,
            options: {
                animation: "slide_from_right",
                headerBackTitleVisible: false,
                headerShown: true,
                headerTransparent: false,
                title: ""
            },
        }
    ];

    return (
        <Stack.Navigator
            initialRouteName={NAVIGATION_HOME}
            screenOptions={{ headerShadowVisible: false }}
        >
            {router.map((item, index) => (
                <Stack.Screen key={index} {...item} />
            ))}
        </Stack.Navigator>
    );
};

export default App;