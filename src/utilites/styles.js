import { StyleSheet } from "react-native";
export default StyleSheet.create({
    masonryContainer: {
        flex: 1,
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: "100%"
    },
    masonryContainerColum: {
        height: "auto",
    },
    masonryItemBox: {
        backgroundColor: "#fff",
        borderRadius: 10,
        borderWidth: 1,
        borderColor: "#3333",
        marginBottom: 20,
        overflow: "hidden"
    },
    masonryItemBoxText: {
        padding: 10,
        width: "100%",
    },
    masonryItemBoxThumbImg: {
        height: 184,
    },
    safeAreaView: {
        backgroundColor: "#fff",
        flex: 1,
        paddingLeft: 24,
        paddingRight: 24,
        paddingTop: 7,
    },
    searchInput: {
        flex: 1,
        paddingTop: 30,
    },
    scrollView: {
        flexGrow: 1,
        width: "100%",
    },
});
