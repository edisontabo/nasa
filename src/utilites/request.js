import axios from "axios";

import { END_POINT } from "@ENV"

export const request = async (
    type = "GET",
    param = "",
    data = {},
) => {
    let headers = {};

    headers = {
        "content-type": "application/x-www-form-urlencoded",
    };


    console.log(END_POINT);

    if (type === "GET") {
        try {
            const req = await axios.get(`${END_POINT}${param}`);
            const res = await req;
            return res;

        } catch (e) {

            console.log(" %%%%% ERROR PETICIÓN GET ");
            console.log(`${END_POINT}${param}`);
            console.log(e);
            console.log(" %%%%% END, ERROR PETICIÓN GET ");

        }
    } else {

        try {
            const req = await axios.post(`${END_POINT}${param}`, data, {
                headers,
            });
            const res = await req;
            return res.data;

        } catch (e) {

            console.log(" %%%%% ERROR PETICIÓN POST ");
            console.log(`${END_POINT}${param}`);
            console.log(e);
            console.log(" %%%%% END, ERROR PETICIÓN POST ");

        }
    }
};
